#include <stm32l476xx.h>
int main(void)
{
 RCC->AHB2ENR    |=0x1;  
 RCC->APB1ENR1 |=0x1; // ->TIM2EN;
int Tim1Prescaler= (uint16_t) (SystemCoreClock/550) - 1; 
int Period = 550-1;
GPIOA->MODER &= ~(0x800); //set first bit
 GPIOA->OTYPER   &= 0x0C000000; // 00
 GPIOA->OSPEEDR  ^= 0xFFFFFFFF; //11
 GPIOA->PUPDR    &= 0x64000000; //00
GPIOA->ODR    =0x0020;
 //GPIOA->MODER &=0xfc; 
 

TIM2->SR   |=0x2;
TIM2->PSC =Tim1Prescaler;
TIM2->ARR =  Period; // 0 - ARR then cnt is set to 0 again //periodas 55s
TIM2->DIER =TIM_DIER_UIE; // set the update interrupt
TIM2->CR1 |=TIM_CR1_CEN;//timer options, with this register we need to start the timer
NVIC_EnableIRQ(TIM2_IRQn); // Enable interrupt from TIM2 (NVIC level)

}

 void TIM2_IRQHandler(void)
  {
if(TIM2->SR & TIM_SR_UIF){
  // GPIOA->ODR  |= 1 << 1;
  //GPIOA->ODR &= ~(1 << 1);
   TIM2->SR &= ~TIM_SR_UIF;
 GPIOA->ODR  ^= 1 << 5; // 5
}
}